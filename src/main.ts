import "bootswatch/dist/cosmo/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import Vue from "vue";
import BootstrapVue from "bootstrap-vue";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faInstagram, faFacebook } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

import App from "./App.vue";

library.add(faInstagram, faFacebook);

Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.use(BootstrapVue);

new Vue({
    render: (h) => h(App),
}).$mount("#app");
